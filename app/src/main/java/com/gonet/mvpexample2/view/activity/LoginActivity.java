package com.gonet.mvpexample2.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;

import com.gonet.mvpexample2.R;
import com.gonet.mvpexample2.contract.LoginContract;
import com.gonet.mvpexample2.model.LoginModel;
import com.gonet.mvpexample2.presenter.LoginPresenter;

public class LoginActivity extends AppCompatActivity implements LoginContract.ViewForPresenter {

    private LoginPresenter presenter;
    private LoginModel model;

    private EditText usernameEditText;
    private EditText passwordEditText;
    private Button loginButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        presenter = new LoginPresenter();
        model = new LoginModel();

        presenter.bindView(this);
        presenter.bindModel(model);

        model.bindPresenter(presenter);

        usernameEditText = findViewById(R.id.activity_login_edit_text_username);

        passwordEditText = findViewById(R.id.activity_login_edit_text_password);

        loginButton = findViewById(R.id.activity_login_button_login);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        presenter.unbind();
        model.unbind();

        presenter = null;
        model = null;
    }

    @Override
    public String getUsername() {
        return usernameEditText.getText().toString();
    }

    @Override
    public String getPassword() {
        return passwordEditText.getText().toString();
    }
}
