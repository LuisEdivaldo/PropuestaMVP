package com.gonet.mvpexample2.model;

import com.gonet.mvpexample2.contract.LoginContract;

public class LoginModel implements LoginContract.ModelForPresenter {

    private LoginContract.PresenterForModel presenter;

    @Override
    public void bindPresenter(LoginContract.PresenterForModel presenter) {
        this.presenter = presenter;
    }

    @Override
    public void unbind() {
        presenter = null;
    }

    @Override
    public void login(String username, String password) {

    }
}
