package com.gonet.mvpexample2.contract;

public interface LoginContract {

    interface ViewForPresenter {
        public String getUsername();
        public String getPassword();
    }

    interface PresenterForView {
        public void bindView(ViewForPresenter view);
        public void bindModel(ModelForPresenter model);
        public void unbind();

        public void onClickLogin();
    }

    interface PresenterForModel {
        public void onSuccessfulLogin();
    }

    interface ModelForPresenter {
        public void bindPresenter(PresenterForModel presenter);
        public void unbind();

        public void login(String username, String password);
    }

}
