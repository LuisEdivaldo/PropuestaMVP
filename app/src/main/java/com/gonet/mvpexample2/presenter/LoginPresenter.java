package com.gonet.mvpexample2.presenter;

import com.gonet.mvpexample2.contract.LoginContract;

public class LoginPresenter implements LoginContract.PresenterForView, LoginContract.PresenterForModel {

    private LoginContract.ViewForPresenter view;
    private LoginContract.ModelForPresenter model;

    @Override
    public void bindView(LoginContract.ViewForPresenter view) {
        this.view = view;
    }

    @Override
    public void bindModel(LoginContract.ModelForPresenter model) {
        this.model = model;
    }

    @Override
    public void unbind() {
        view = null;
        model = null;
    }

    @Override
    public void onClickLogin() {

    }

    @Override
    public void onSuccessfulLogin() {

    }
}
